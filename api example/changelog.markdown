Framework Release Notes and Changes
==========================
*v1.8*
-------
* Removed the need for including the ChromaService in your application manifest. The project is now a pure android-library project.

* Change to using the library format aar, which is not supported by eclipse.

* Added Matching straight LAB colors from a stored chroma scan.

* Various bug fixes

* Decoupled the ChromaCalibrationResult out into its own registration event. See DefaultNotifer.instance().addChromaCalibrationResultListener(...)

* Major Bug Fix with BluetoothService.killConnection, no longer stalls forever.

* Added Firmware out of date class for auto registering an application to toast message when a connected device's firmware is out of date.

*v1.7*
-------
* Application's Android Manifest requires this inside the <application> tag
```java
        <service android:name="com.variable.framework.chroma.service.ChromaService" android:process=".ChromaService" >
            <intent-filter>
                <action android:name="com.variable.chroma.action.StartChromaService" />
                <category android:name="android.intent.category.DEFAULT" />
            </intent-filter>
        </service>
```

* Changed ChromaDevice.ChromaListener.onChromaReadingReceived methods from ChromaDevice parameters to parcelable ChromaModuleInfo type.

* Removed the need for Client code to start initializing chroma device.

* Added internal support for Chroma offline usuage through using Messenger service.

* Added Chroma Match functionality by using the ChromaServiceClient.

* Added programmatic pairing at connection time. Specifically, when a connection attempt is made to a unbonded/paired NODE, the framework will now attempt
to programmatic bond. If this fails the new callback onPairingFailure(NodeDevice) in ConnectionListener will be invoked.

* Added discovery functionality. The onNODEDiscovered callback in ConnectionListener will now be invoked when a new bluetooth device is discovered.
For this feature to work it is required that the application pass its application context instance to NodeApplication.initialize(Context).

* Added ChromaService, which now supports ChromaMatch functionality that is available in the Objective-C framework.

* Removed ChromaBatchingAndCalibrationTask, DataMode.

* Bug fix for Clima datalogging timestamps.

* Bug fixes for multiple Oxa streaming.

* Added onFirmwareUpdateRequired(NodeDevice) to the StatusListener. This callback is invoked if the NODE's firmware is outdated. Further, this check is made whenever a request for a NODE's firmware/version.

* Added High Speed Acceleration collection. (Example is TBC)

*v1.6*
-------

* StatusListener, ChromaListener, ButtonListener, SensorDetector, DataloggingListener, and ConnectionListener are now invoked on the UI Thread.

* DeltaE CMC 2:1 and CMC 1:1 are now fully supported in VTRGBCReading class.

* Fixed model property on NodeDevice to match product name.

* Fixed null pointer for motion sensor after a NodeDevice has been initialized.

* ConnectionListener callback are now supported by the bluetooth service class.

* More bluetooth enhancements

* Added a ConnectionFail event to ConnectionListener and supported in BluetoothService.

* Added a ConnectionFail message to be sent through the handler.

* Bug Fixes for Chroma Readings.

* Removed onStatusChanged from ConnectionListener.

* Deprecated methods on StatusListener. Register a sensor detector for those messages.

* Fixed NodeDevice QuietMode member to accurately reflect the state of the NODE.

* Reduced the amount of threads to 1 used by the DefaultNotifier.

*v1.5*
------
* Updated ProgressListener callback to accept text based message updates rather than incremental progress updates.

* Added more safety catches to dispatching events allows for more verbose messaging to application code.

* All Bluetooth writes are made asynchrounously.