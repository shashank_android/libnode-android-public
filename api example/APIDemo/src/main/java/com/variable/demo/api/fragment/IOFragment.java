package com.variable.demo.api.fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.variable.demo.api.R;
import com.variable.demo.api.NodeApplication;
import com.variable.framework.dispatcher.DefaultNotifier;
import com.variable.framework.node.IOSensor;
import com.variable.framework.node.NodeDevice;
import com.variable.framework.node.enums.NodeEnums;


public class IOFragment extends Fragment implements IOSensor.IOEventListener {
    public static final String TAG = IOFragment.class.getName();
    private SeekBar mA0SeekBar;
    private SeekBar mA1SeekBar;
    private View[] gpioViews = new View[8];

    private IOSensor mIO;
    private IOSensor.PinConfigType[] pinTypes = {
            IOSensor.PinConfigType.GP_I,IOSensor.PinConfigType.GP_I,IOSensor.PinConfigType.GP_I,IOSensor.PinConfigType.GP_I,
            IOSensor.PinConfigType.GP_I,IOSensor.PinConfigType.GP_I,IOSensor.PinConfigType.GP_I,IOSensor.PinConfigType.GP_I};

    private final Handler mUiThreadHandler = new Handler(Looper.getMainLooper());

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_io, null, false);

        //IO Buttons:
        final int[] gpios = {R.id.gpio7,R.id.gpio6,R.id.gpio5,R.id.gpio4,R.id.gpio3,R.id.gpio2,R.id.gpio1,R.id.gpio0};
        for (int i=0;i<8;i++)
        {
            final int gpioNum = i;
            View gpio = root.findViewById(gpios[i]);
            gpioViews[i] = gpio;

            ((TextView)gpio.findViewById(R.id.textViewGpio)).setText("GPIO D" + (7-i));
            RadioGroup gpc = (RadioGroup)gpio.findViewById(R.id.radioGroupGPC);
            gpc.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
            {
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    onRadioButtonClicked(group, checkedId, gpioNum);
                }
            });

            final ToggleButton onOff = (ToggleButton)gpio.findViewById(R.id.toggle);
            onOff.setEnabled(false);
            onOff.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    byte d7_to_d0 = 0x00;
                    if (onOff.isChecked()){
                        d7_to_d0 = (byte)(1<<(7-gpioNum));
                    }
                    mIO.setIOPin((byte)(1<<(7-gpioNum)),d7_to_d0);
                }
            });
        }

        //A2D Seek Bar Setup
        mA0SeekBar = (SeekBar) root.findViewById(R.id.seekBar0);
        mA0SeekBar.setProgress(30);
        mA0SeekBar.setMax(100);
        mA0SeekBar.setEnabled(false);
        mA1SeekBar = (SeekBar) root.findViewById(R.id.seekBar1);
        mA1SeekBar.setProgress(30);
        mA1SeekBar.setMax(100);
        mA1SeekBar.setEnabled(false);

        //UART Button:
        Button goToUart = (Button)root.findViewById(R.id.btnGo2UartFragment);
        goToUart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment existingFrag = getFragmentManager().findFragmentByTag(IOUARTFragment.TAG);
                if(existingFrag != null){
                    getFragmentManager().beginTransaction().remove(existingFrag).commitAllowingStateLoss();
                }

                ft.replace(R.id.center_fragment_container, new IOUARTFragment(), IOUARTFragment.TAG);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        return root;
    }


    @Override
    public void onResume() {
        super.onResume();

        DefaultNotifier.instance().addIOListener(this);
        NodeDevice node = NodeApplication.getActiveNode();
        if(node != null)
        {
            mIO = node.findSensor(NodeEnums.ModuleType.IO);
            try {
                mIO.setIOPinConfig(pinTypes);
            } catch (Exception e) {
                e.printStackTrace();
            }
            boolean streamGPIO = true;
            boolean streamA2D0 = true;
            boolean streamA2D1 = true;
            int gainA0 = 1; int gainA1 = 1;//currently only A2D gain = 1 supported.
            int intervalx10ms = 0;//fastest;
            int lifetimex10ms = 0;//forever
            mIO.setStreamMode(streamGPIO, streamA2D0, gainA0, streamA2D1, gainA1, intervalx10ms, lifetimex10ms);

            //stream GPIO values = on
            //stream A2D0 & A2D1 = on
            //period = fastest (0) - max theoretical update speed would be 10ms, reality will be slower here
            //lifetime = inf (0)
            //mIO.startSensor();  //equivalent to setStreamMode(true,true,1,true,1,0,0):
            //NOTE - you don't have to stream to get IO Module values,  IO state can be read explicitly.
        }
    }


    @Override
    public void onPause() {
        super.onPause();

        DefaultNotifier.instance().removeIOListener(this);
        //Turn off io streaming (power, everything)
        mIO.stopSensor();

    }



    public void onRadioButtonClicked(RadioGroup group, int checkedId, int gpioNum) {
        // checkedId is the RadioButton selected
        RadioButton rb =(RadioButton) group.findViewById(checkedId);
        ToggleButton onOff = (ToggleButton)gpioViews[gpioNum].findViewById(R.id.toggle);

        switch(checkedId) {
            case R.id.radioIn:
                if(rb.isChecked()) {
                    onOff.setEnabled(false);
                    //TODO: update after API updated (to GPI)
                    pinTypes[gpioNum] = IOSensor.PinConfigType.GP_I;
                }
                break;
            case R.id.radioOut:
                if(rb.isChecked()) {
                    onOff.setEnabled(true);
                    //TODO: update after API updated (to GPO)
                    pinTypes[gpioNum] = IOSensor.PinConfigType.GP_O;
                }
                break;
            case R.id.radioIrq:
                if(rb.isChecked()) {
                    //TODO: update after API updated (to IRQ)
                    pinTypes[gpioNum] = IOSensor.PinConfigType.IRQ;
                    onOff.setEnabled(false);
                }
                break;
        }
        try {
            mIO.setIOPinConfig(pinTypes);
        }catch (Exception e){
            e.printStackTrace();
        }
    }




    @Override
    public void transmittedPinStatus(IOSensor ioSensor, byte b) {
        final byte fb = b;

        mUiThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i<8;i++)
                {
                    ToggleButton onOff = (ToggleButton)gpioViews[i].findViewById(R.id.toggle);
                    if ( ((1<<(7-i)) & fb) == (1<<(7-i)) ) {
                        onOff.setChecked(true);
                    }
                    else {
                        onOff.setChecked(false);
                    }
                }
            }
        });

    }

    @Override
    public void transmittedIrq(IOSensor ioSensor, byte b) {
        final byte fb = b;
        mUiThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i<8;i++)
                {
                    ToggleButton onOff = (ToggleButton)gpioViews[i].findViewById(R.id.toggle);
                    if ( ((1<<(7-i)) & fb) == (1<<(7-i)) )
                    {
                        onOff.setChecked(true);
                    }
                }
            }
        });
    }

    @Override
    public void transmittedA2DState(IOSensor ioSensor, byte b, float v) {
        final boolean a0_or_a1 = (b == 1) ? true : false;
        final int a2dVal = (int)(v*100);
        mUiThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                if (!a0_or_a1){
                    mA0SeekBar.setProgress(a2dVal);
                }else{
                    mA1SeekBar.setProgress(a2dVal);
                }
            }
        });

    }




    @Override
    public void transmittedUartBuffer(IOSensor ioSensor, byte[] bytes) {
        //UART not shown in this fragment
    }




}
