package com.variable.demo.api.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
/* See http://variableinc.com/terms-use-license for the full license governing this code. */
import android.widget.Toast;
import android.widget.ToggleButton;

import com.variable.demo.api.NodeApplication;
import com.variable.demo.api.R;
import com.variable.framework.dispatcher.DefaultNotifier;
import com.variable.framework.node.IOSensor;
import com.variable.framework.node.NodeDevice;
import com.variable.framework.node.enums.NodeEnums;

import java.io.UnsupportedEncodingException;

/**
 *
 * The IO Module's UARTFragment lets the user send strings over IO UART
 * For a quick test, the UART RX and TX pins on IO module can be connected directly together to see your messages echo back.
 *
 * Created by andrewt on 2/15/15.
 *
 */
public class IOUARTFragment extends Fragment implements IOSensor.IOEventListener  {
    public static final String TAG = IOUARTFragment.class.getName();
    private IOSensor mIO;
    private EditText mTextInput;
    private TextView mTextHistory;
    private Context context;

    private final Handler mUiThreadHandler = new Handler(Looper.getMainLooper());

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        context = this.getActivity();
        View root = inflater.inflate(R.layout.fragment_io_uart, null, false);
        mTextHistory = (TextView)root.findViewById(R.id.textViewUartHistory);
        mTextInput = (EditText)root.findViewById(R.id.editTextUartIn);
        mTextInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String inputString = s.toString();
                if(inputString != null && inputString != "")
                {
                    if( -1 != inputString.indexOf( "\n" ) ) {
                        mTextHistory.append("Me:" + inputString + "\n\n");
                        try {
                            byte[] bytes = inputString.getBytes("US-ASCII");
                            mIO.sendUARTBuffer(bytes);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        mTextInput.setText("");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        DefaultNotifier.instance().addIOListener(this);
        NodeDevice node = NodeApplication.getActiveNode();
        if(node != null)
        {
            mIO = node.findSensor(NodeEnums.ModuleType.IO);
            mIO.requestUARTBuffer(9600,false);//setup/start UART for 9600 Baud w/ no flowcontrol
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        DefaultNotifier.instance().removeIOListener(this);
        //Turn off io (power, everything)
        mIO.stopSensor();
    }

    @Override
    public void transmittedUartBuffer(IOSensor ioSensor, byte[] bytes) {
        //convert byte to string:
        try {
            final String text = new String(bytes, "US-ASCII");
            mUiThreadHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, text, Toast.LENGTH_LONG).show();
                    mTextHistory.append("NODE:" + text + "\n\n");
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void transmittedPinStatus(IOSensor ioSensor, byte b) {
        //not using GPIO in this view
    }

    @Override
    public void transmittedIrq(IOSensor ioSensor, byte b) {
        //not using GPIO in this view
    }

    @Override
    public void transmittedA2DState(IOSensor ioSensor, byte b, float v) {
        //not using A2D in this view
    }
}
