Framework Release Notes and Changes
==========================
*v1.8*
-------
* Removed the need for including the ChromaService in your application manifest. The project is now a pure android-library project.

* Change to using the library format aar, which is not supported by eclipse.

* Added Matching straight LAB colors from a stored chroma scan.

* Various bug fixes

* Decoupled the ChromaCalibrationResult out into its own registration event. See DefaultNotifier.instance().addChromaCalibrationResultListener(...)

* Major Bug Fix with BluetoothService.killConnection, no longer stalls forever.

* Added Firmware out of date class for auto registering an application to toast message when a connected device's firmware is out of date.